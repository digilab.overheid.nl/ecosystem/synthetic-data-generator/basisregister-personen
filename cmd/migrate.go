package cmd

import (
	"context"
	"fmt"
	"log"

	_ "github.com/golang-migrate/migrate/v4/database/postgres" // database driver
	_ "github.com/lib/pq"                                      // database driver
	"github.com/spf13/cobra"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-backend/pkg/storage"
)

var migrateOpts struct { //nolint:gochecknoglobals // this is the recommended way to use cobra
	PostgresDSN string
}

//nolint:gochecknoinits // this is the recommended way to use cobra
func init() {
	migrateUpCommand.Flags().StringVarP(&migrateOpts.PostgresDSN, "sdg-postgres-dsn", "", "", "Postgres Connection URL")
	migrateInitCommand.Flags().StringVarP(&migrateOpts.PostgresDSN, "sdg-postgres-dsn", "", "", "Postgres Connection URL")

	if err := migrateUpCommand.MarkFlagRequired("sdg-postgres-dsn"); err != nil {
		log.Fatal(err)
	}

	if err := migrateInitCommand.MarkFlagRequired("sdg-postgres-dsn"); err != nil {
		log.Fatal(err)
	}

	migrateCommand.AddCommand(migrateUpCommand)
	migrateCommand.AddCommand(migrateInitCommand)
}

var migrateCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "migrate",
	Short: "Run the migration tool",
	Run:   func(cmd *cobra.Command, args []string) {},
}

var migrateUpCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "up",
	Short: "Migrate the API",
	Run: func(cmd *cobra.Command, args []string) {
		if err := storage.PostgresPerformMigrations(migrateOpts.PostgresDSN, PSQLSchemaName); err != nil {
			log.Fatalf(fmt.Errorf("failed to perform migrations: %w", err).Error())
		}

		fmt.Println("Migrate up successful")
	},
}

var migrateInitCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "init",
	Short: "Init the API",
	Run: func(cmd *cobra.Command, args []string) {
		if err := migrateInit(migrateOpts.PostgresDSN); err != nil {
			log.Fatalf(fmt.Errorf("failed to migrate init: %w", err).Error())
		}
	},
}

func migrateInit(dns string) error {
	db, err := storage.NewPostgreSQLConnection(dns)
	if err != nil {
		return fmt.Errorf("failed to create new config: %w", err)
	}

	if _, err := db.ExecContext(context.Background(), fmt.Sprintf("CREATE SCHEMA IF NOT EXISTS %s", PSQLSchemaName)); err != nil {
		return fmt.Errorf("failed to create schema: %w", err)
	}

	fmt.Println("migrate init successful")

	return nil
}
