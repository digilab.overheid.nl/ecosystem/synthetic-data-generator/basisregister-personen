package cmd

import "github.com/spf13/cobra"

const (
	PSQLSchemaName = "digilab_demo_frp"
)

var RootCmd = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "api",
	Short: "API",
}

func Execute() error {
	return RootCmd.Execute() //nolint:wrapcheck // unnecessary
}

//nolint:gochecknoinits // this is the recommended way to use cobra
func init() {
	RootCmd.AddCommand(serveCommand)
	RootCmd.AddCommand(migrateCommand)
}
