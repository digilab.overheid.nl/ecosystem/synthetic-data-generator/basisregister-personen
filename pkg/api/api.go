package api

import (
	"net/http"
	"time"

	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-backend/pkg/storage"
)

type API struct {
	*http.Server
	db *storage.Database
}

type NewAPIArgs struct {
	DB            *storage.Database
	ListenAddress string
}

func New(args *NewAPIArgs) (*API, error) {
	api := &API{
		db: args.DB,
	}

	const (
		ReadTimeout  = 30 * time.Second
		WriteTimeout = 30 * time.Second
	)

	api.Server = &http.Server{
		Addr:         args.ListenAddress,
		Handler:      Router(api),
		ReadTimeout:  ReadTimeout,
		WriteTimeout: WriteTimeout,
	}

	return api, nil
}
