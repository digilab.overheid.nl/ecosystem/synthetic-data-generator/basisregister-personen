//nolint:nlreturn // makes the code less readable
package api

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-backend/pkg/meta"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-backend/pkg/model"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-backend/pkg/pagination"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-backend/pkg/storage/adapter"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-backend/pkg/storage/queries/generated"
)

func (a *API) PeopleList(w http.ResponseWriter, r *http.Request) {
	md, err := meta.New(r)
	if err != nil {
		writeError(w, err)
		return
	}

	var records []*queries.PeopleGetRow

	switch md.IsASC() {
	case true:
		records, err = a.peopleListAsc(r.Context(), md)
	case false:
		records, err = a.peopleListDesc(r.Context(), md)
	}

	if err != nil {
		writeError(w, err)
		return
	}

	people := make([]*model.Person, 0, len(records))

	for idx := range records {
		person, err := adapter.ToPeople(records[idx])
		if err != nil {
			writeError(w, err)
			return
		}

		people = append(people, person)
	}

	response, err := pagination.Build(people, md)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(response); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (a *API) peopleListAsc(ctx context.Context, md *meta.Data) ([]*queries.PeopleGetRow, error) {
	params := &queries.PeopleListAscParams{
		ID:    md.FirstID,
		Limit: int32(md.PerPage + 1),
	}

	records, err := a.db.Queries.PeopleListAsc(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("people list failed: %w", err)
	}

	people := make([]*queries.PeopleGetRow, 0, len(records))
	for idx := range records {
		people = append(people, (*queries.PeopleGetRow)(records[idx]))
	}

	return people, nil
}

func (a *API) peopleListDesc(ctx context.Context, md *meta.Data) ([]*queries.PeopleGetRow, error) {
	params := &queries.PeopleListDescParams{
		ID:         md.LastID,
		InternalID: math.MaxInt32,
		Limit:      int32(md.PerPage + 1),
	}

	records, err := a.db.Queries.PeopleListDesc(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("people list failed: %w", err)
	}

	people := make([]*queries.PeopleGetRow, 0, len(records))
	for idx := range records {
		people = append(people, (*queries.PeopleGetRow)(records[idx]))
	}

	return people, nil
}

func (a *API) PeopleListAddress(w http.ResponseWriter, r *http.Request) {
	addressID, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	records, err := a.db.Queries.PeopleListAddress(r.Context(), addressID)
	if err != nil {
		writeError(w, err)
		return
	}

	people := make([]model.Person, 0, len(records))

	for idx := range records {
		person, err := adapter.ToPeople((*queries.PeopleGetRow)(records[idx]))
		if err != nil {
			writeError(w, err)
			return
		}

		people = append(people, *person)
	}

	if err := json.NewEncoder(w).Encode(people); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (a *API) PeopleGet(w http.ResponseWriter, r *http.Request) {
	bsn := chi.URLParam(r, "id")

	var person *model.Person

	var err error

	id, parseErr := uuid.Parse(bsn)

	switch {
	case parseErr != nil:
		person, err = a.PeopleGetBSN(r.Context(), bsn)
	case parseErr == nil:
		person, err = a.PeopleGetID(r.Context(), id)
	}

	if err != nil {
		writeError(w, err)
		return
	}

	parents, err := a.peopleGetParents(r.Context(), person.ID)
	if err != nil {
		writeError(w, err)
		return
	}

	if len(parents) > 0 {
		person.Parents = parents
	}

	partners, err := a.peopleGetPartners(r.Context(), person.ID)
	if err != nil {
		writeError(w, err)
		return
	}

	if len(parents) > 0 {
		person.Partners = partners
	}

	children, err := a.peopleGetChildren(r.Context(), person.ID)
	if err != nil {
		writeError(w, err)
		return
	}

	if len(children) > 0 {
		person.Children = children
	}

	w.Header().Set("FSC-VWL-verwerkings-activiteit", "http://example.com/activity/v0/get_person")
	w.Header().Set("FSC-VWL-verwerkings-span", uuid.New().String())
	w.Header().Set("FSC-VWL-systeem", "fictief-register-personen-backend")
	w.Header().Set("FSC-VWL-verantwoordelijke", "woudendijk")
	w.Header().Set("FSC-VWL-verwerker", "frp-provider")

	if err := json.NewEncoder(w).Encode(person); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (a *API) PeopleGetMany(w http.ResponseWriter, r *http.Request) {
	var data struct {
		IDs  []uuid.UUID `json:"ids"`
		BSNs []string    `json:"bsns"`
	}
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		writeError(w, err)
		return
	}

	fmt.Printf("data: %v\n", data)

	if len(data.IDs)+len(data.BSNs) > 1000 {
		writeError(w, errors.New("too many IDs requested, should be at most 1000"))
		return
	}

	var people []model.Person

	if len(data.IDs) != 0 {
		records, err := a.db.Queries.PeopleGetMany(r.Context(), data.IDs)
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				w.WriteHeader(http.StatusNotFound)
				return
			}

			writeError(w, fmt.Errorf("people get many: %w", err))
			return
		}

		p, err := adapter.ToManyPeople(records)
		if err != nil {
			writeError(w, fmt.Errorf("error converting people: %w", err))
			return
		}

		people = append(people, p...)
	}

	if len(data.BSNs) != 0 {
		records, err := a.db.Queries.PeopleGetBsnMany(r.Context(), data.BSNs)
		if err != nil {
			writeError(w, fmt.Errorf("people get bsn many: %w", err))
			return
		}

		p, err := adapter.ToManyPeopleBsn(records)
		if err != nil {
			writeError(w, fmt.Errorf("error converting people: %w", err))
			return
		}

		people = append(people, p...)
	}

	if err := json.NewEncoder(w).Encode(people); err != nil {
		writeError(w, fmt.Errorf("encoding failed: %w", err))
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (a *API) PeopleGetBSN(ctx context.Context, bsn string) (*model.Person, error) {
	record, err := a.db.Queries.PeopleGetBsn(ctx, sql.NullString{String: bsn, Valid: true})
	if err != nil {
		return nil, err
	}

	return adapter.ToPeople((*queries.PeopleGetRow)(record))
}

func (a *API) PeopleGetID(ctx context.Context, id uuid.UUID) (*model.Person, error) {
	record, err := a.db.Queries.PeopleGet(ctx, id)
	if err != nil {
		return nil, err
	}

	return adapter.ToPeople(record)
}

func (a *API) peopleGetParents(ctx context.Context, id uuid.UUID) ([]model.Person, error) {
	recordParents, err := a.db.Queries.PeopleGetParents(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("people get parents failed: %w", err)
	}

	parents := make([]model.Person, 0, len(recordParents))

	for idx := range recordParents {
		person, err := adapter.ToPeople((*queries.PeopleGetRow)(recordParents[idx]))
		if err != nil {
			return nil, fmt.Errorf("adapter to people failed: %w", err)
		}

		parents = append(parents, *person)
	}

	return parents, nil
}

func (a *API) peopleGetPartners(ctx context.Context, id uuid.UUID) ([]model.Person, error) {
	record, err := a.db.Queries.PeopleGetPartners(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("people get partners failed: %w", err)
	}

	partners := make([]model.Person, 0, len(record))

	for idx := range record {
		person, err := adapter.ToPeople((*queries.PeopleGetRow)(record[idx]))
		if err != nil {
			return nil, fmt.Errorf("adapter to people failed: %w", err)
		}

		partners = append(partners, *person)
	}

	return partners, nil
}

func (a *API) peopleGetChildren(ctx context.Context, id uuid.UUID) ([]model.Person, error) {
	records, err := a.db.Queries.PeopleGetChildren(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("people get children failed: %w", err)
	}

	children := make([]model.Person, 0, len(records))

	for idx := range records {
		person, err := adapter.ToPeople((*queries.PeopleGetRow)(records[idx]))
		if err != nil {
			return nil, fmt.Errorf("adapter to people failed: %w", err)
		}

		children = append(children, *person)
	}

	return children, nil
}

func (a *API) PeopleCreate(w http.ResponseWriter, r *http.Request) {
	person := &model.Person{}
	if err := json.NewDecoder(r.Body).Decode(person); err != nil {
		writeError(w, fmt.Errorf("decode failed: %w", err))
		return
	}

	args, err := adapter.FromPeople(person)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := a.db.Queries.PeopleCreate(r.Context(), args); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (a *API) PeopleUpdate(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	person := &model.Person{}
	if err := json.NewDecoder(r.Body).Decode(person); err != nil {
		writeError(w, err)
		return
	}

	args, err := adapter.FromPeopleUpdate(id, person)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := a.db.Queries.PeopleUpdate(r.Context(), args); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (a *API) PeoplePatch(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	patch := &model.PersonPatch{}
	if err := json.NewDecoder(r.Body).Decode(patch); err != nil {
		writeError(w, err)
		return
	}

	record, err := a.db.Queries.PeopleGet(r.Context(), id)
	if err != nil {
		writeError(w, err)
		return
	}

	person, err := adapter.ToPeople(record)
	if err != nil {
		writeError(w, err)
		return
	}

	args, err := adapter.FromPeoplePatch(id, person, patch)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := a.db.Queries.PeopleUpdate(r.Context(), args); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (a *API) PeopleMarry(w http.ResponseWriter, r *http.Request) {
	marry := &model.Marry{}
	if err := json.NewDecoder(r.Body).Decode(marry); err != nil {
		writeError(w, err)
		return
	}

	args := &queries.PeopleMarryParams{
		Person1ID: marry.Person1,
		Person2ID: marry.Person2,
	}

	if err := a.db.Queries.PeopleMarry(r.Context(), args); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (a *API) PeopleAddChild(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	addChild := &model.AddChild{}
	if err := json.NewDecoder(r.Body).Decode(addChild); err != nil {
		writeError(w, err)
		return
	}

	args := &queries.PeopleAddChildParams{
		PersonID: addChild.Person,
		ParentID: id,
	}

	if err := a.db.Queries.PeopleAddChild(r.Context(), args); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (a *API) PeopleDelete(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	if err = a.db.Queries.PeopleDelete(r.Context(), id); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}
