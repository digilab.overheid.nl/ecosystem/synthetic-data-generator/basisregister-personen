package model

import (
	"time"

	"github.com/google/uuid"
)

type Person struct {
	ID                  uuid.UUID  `json:"id"`
	BSN                 string     `json:"bsn,omitempty"`
	Name                string     `json:"name,omitempty"`
	BornAt              *time.Time `json:"bornAt,omitempty"`
	Sex                 string     `json:"sex,omitempty"`
	DiedAt              *time.Time `json:"diedAt,omitempty"`
	RegisteredAddressID *uuid.UUID `json:"registeredAddressId,omitempty"`
	Partners            []Person   `json:"partners,omitempty"`
	Children            []Person   `json:"children,omitempty"`
	Parents             []Person   `json:"parents,omitempty"`
}

func (person *Person) GetID() uuid.UUID {
	return person.ID
}

type PersonPatch struct {
	BSN                 *string    `json:"bsn"`
	Name                *string    `json:"name"`
	BornAt              *time.Time `json:"bornAt"`
	Sex                 *string    `json:"sex"`
	DiedAt              *time.Time `json:"diedAt"`
	RegisteredAddressID *uuid.UUID `json:"registeredAddressId"`
}

type Marry struct {
	Person1 uuid.UUID `json:"person1"`
	Person2 uuid.UUID `json:"person2"`
}

type AddChild struct {
	Person uuid.UUID `json:"person"`
}
