-- name: PeopleListAsc :many
SELECT p.id, p.internal_id, p.bsn, p.name, p.born_at, p.sex, p.died_at, p.registered_address_id FROM (
    SELECT id, internal_id, bsn, name, born_at, sex, died_at, registered_address_id
    FROM digilab_demo_frp.people
    WHERE internal_id > (
        SELECT sub_qry_people.internal_id
        FROM digilab_demo_frp.people as sub_qry_people
        WHERE sub_qry_people.id = $1
    )
    ORDER BY internal_id ASC
    LIMIT $2
) p order by p.internal_id DESC;

-- name: PeopleListDesc :many
SELECT id, internal_id, bsn, name, born_at, sex, died_at, registered_address_id
FROM digilab_demo_frp.people
WHERE people.internal_id < COALESCE((
    SELECT internal_id
    FROM digilab_demo_frp.people as sub_qry_people
    WHERE sub_qry_people.id = $1
), $2)
ORDER BY people.internal_id DESC
LIMIT $3;

-- name: PeopleListAddress :many
SELECT id, internal_id, bsn, name, born_at, sex, died_at, registered_address_id
FROM digilab_demo_frp.people
WHERE registered_address_id = @address_id::uuid;

-- name: PeopleGet :one
SELECT id, internal_id, bsn, name, born_at, sex, died_at, registered_address_id
FROM digilab_demo_frp.people
WHERE id = $1;

-- name: PeopleGetMany :many
SELECT id, internal_id, bsn, name, born_at, sex, died_at, registered_address_id
FROM digilab_demo_frp.people
WHERE id = ANY($1::uuid[]);

-- name: PeopleGetBsnMany :many
SELECT id, internal_id, bsn, name, born_at, sex, died_at, registered_address_id
FROM digilab_demo_frp.people
WHERE bsn = ANY($1::TEXT[]);

-- name: PeopleGetBsn :one
SELECT id, internal_id, bsn, name, born_at, sex, died_at, registered_address_id
FROM digilab_demo_frp.people
WHERE bsn = $1;

-- name: PeopleGetPartners :many
SELECT id, internal_id, bsn, name, born_at, sex, died_at, registered_address_id
FROM digilab_demo_frp.people
LEFT JOIN digilab_demo_frp.partners as partners
    ON partners.person_1_id=$1
    OR partners.person_2_id=$1
WHERE id = partners.person_1_id AND id != $1 OR
    id = partners.person_2_id AND id != $1;

-- name: PeopleGetChildren :many
SELECT id, internal_id, bsn, name, born_at, sex, died_at, registered_address_id
FROM digilab_demo_frp.people
LEFT JOIN digilab_demo_frp.parents as parents
    ON parents.parent_id=$1
WHERE id=parents.person_id;

-- name: PeopleGetParents :many
SELECT id, internal_id, bsn, name, born_at, sex, died_at, registered_address_id
FROM digilab_demo_frp.people
LEFT JOIN digilab_demo_frp.parents as parents
    ON parents.person_id=$1
WHERE id=parents.parent_id;

-- name: PeopleCreate :exec
INSERT INTO digilab_demo_frp.people (
    id, bsn, name, born_at, sex, died_at, registered_address_id
)
VALUES ($1, $2, $3, $4, $5, $6, $7);

-- name: PeopleUpdate :exec
UPDATE digilab_demo_frp.people
SET bsn=$1, name=$2, born_at=$3, sex=$4, died_at=$5, registered_address_id=$6
WHERE id=$7;

-- name: PeopleMarry :exec
INSERT INTO digilab_demo_frp.partners (
    person_1_id, person_2_id
)
VALUES ($1, $2);

-- name: PeopleAddChild :exec
INSERT INTO digilab_demo_frp.parents (
    person_id, parent_id
)
VALUES ($1, $2);

-- name: PeopleDelete :exec
DELETE FROM digilab_demo_frp.people
WHERE id=$1;
