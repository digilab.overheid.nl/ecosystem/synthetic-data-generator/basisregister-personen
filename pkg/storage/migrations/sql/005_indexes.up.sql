BEGIN;

CREATE INDEX parents_person_id ON digilab_demo_frp.parents(person_id);
CREATE INDEX parents_parent_id ON digilab_demo_frp.parents(parent_id);

CREATE INDEX partners_person_1_id ON digilab_demo_frp.partners(person_1_id);
CREATE INDEX partners_person_2_id ON digilab_demo_frp.partners(person_2_id);

CREATE INDEX people_address_id ON digilab_demo_frp.people(registered_address_id);
CREATE INDEX people_internal_id ON digilab_demo_frp.people(internal_id);

COMMIT;
