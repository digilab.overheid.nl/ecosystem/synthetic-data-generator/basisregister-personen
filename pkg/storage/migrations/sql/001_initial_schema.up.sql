BEGIN transaction;

CREATE SCHEMA IF NOT EXISTS digilab_demo_frp;

CREATE TABLE digilab_demo_frp.people (
    id UUID NOT NULL,
    bsn TEXT UNIQUE,
    born_at TIMESTAMP,
    sex TEXT,
    name TEXT,
    died_at TIMESTAMP,

    PRIMARY KEY(id)
);

CREATE TABLE digilab_demo_frp.parents (
    person_id UUID NOT NULL,
    parent_id UUID NOT NULL,

    CONSTRAINT fk_person FOREIGN KEY(person_id) REFERENCES digilab_demo_frp.people(id),
    CONSTRAINT fk_parent FOREIGN KEY(parent_id) REFERENCES digilab_demo_frp.people(id)
);

CREATE TABLE digilab_demo_frp.partners (
    person_1_id UUID NOT NULL,
    person_2_id UUID NOT NULL,

    CONSTRAINT fk_person_1 FOREIGN KEY(person_1_id) REFERENCES digilab_demo_frp.people(id),
    CONSTRAINT fk_person_2 FOREIGN KEY(person_2_id) REFERENCES digilab_demo_frp.people(id)
);

CREATE INDEX idx_people_bsn ON digilab_demo_frp.people(bsn);

COMMIT;
