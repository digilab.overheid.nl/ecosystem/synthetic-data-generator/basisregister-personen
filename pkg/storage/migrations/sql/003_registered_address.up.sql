BEGIN TRANSACTION;

ALTER TABLE digilab_demo_frp.people ADD COLUMN registered_address_id UUID;

COMMIT;
