package adapter

import (
	"database/sql"

	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-backend/pkg/model"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-backend/pkg/storage/queries/generated"
)

func ToPeople(record *queries.PeopleGetRow) (*model.Person, error) {
	person := &model.Person{
		ID:   record.ID,
		BSN:  record.Bsn.String,
		Name: record.Name.String,
		Sex:  record.Sex.String,
	}

	if record.RegisteredAddressID.Valid {
		person.RegisteredAddressID = &record.RegisteredAddressID.UUID
	}

	if record.BornAt.Valid {
		person.BornAt = &record.BornAt.Time
	}

	if record.DiedAt.Valid {
		person.DiedAt = &record.DiedAt.Time
	}

	return person, nil
}

func ToManyPeople(records []*queries.PeopleGetManyRow) ([]model.Person, error) {
	// Convert the records to model.Person
	people := make([]model.Person, 0, len(records))

	for idx := range records {
		person, err := ToPeople((*queries.PeopleGetRow)(records[idx]))
		if err != nil {
			return nil, err
		}

		people = append(people, *person)
	}

	return people, nil
}

func ToManyPeopleBsn(records []*queries.PeopleGetBsnManyRow) ([]model.Person, error) {
	// Convert the records to model.Person
	people := make([]model.Person, 0, len(records))

	for idx := range records {
		person, err := ToPeople((*queries.PeopleGetRow)(records[idx]))
		if err != nil {
			return nil, err
		}

		people = append(people, *person)
	}

	return people, nil
}

func FromPeople(person *model.Person) (*queries.PeopleCreateParams, error) {
	args := &queries.PeopleCreateParams{
		ID:   person.ID,
		Bsn:  sql.NullString{String: person.BSN, Valid: true},
		Name: sql.NullString{String: person.Name, Valid: true},
		Sex:  sql.NullString{String: person.Sex, Valid: true},
	}

	if person.RegisteredAddressID != nil {
		args.RegisteredAddressID = uuid.NullUUID{UUID: *person.RegisteredAddressID, Valid: true}
	}

	if person.BornAt != nil {
		args.BornAt = sql.NullTime{Time: *person.BornAt, Valid: true}
	}

	if person.DiedAt != nil {
		args.DiedAt = sql.NullTime{Time: *person.DiedAt, Valid: true}
	}

	return args, nil
}

func FromPeopleUpdate(id uuid.UUID, person *model.Person) (*queries.PeopleUpdateParams, error) {
	args := &queries.PeopleUpdateParams{
		ID:   id,
		Bsn:  sql.NullString{String: person.BSN, Valid: true},
		Name: sql.NullString{String: person.Name, Valid: true},
		Sex:  sql.NullString{String: person.Sex, Valid: true},
	}

	if person.RegisteredAddressID != nil {
		args.RegisteredAddressID = uuid.NullUUID{UUID: *person.RegisteredAddressID, Valid: true}
	}

	if person.BornAt != nil {
		args.BornAt = sql.NullTime{Time: *person.BornAt, Valid: true}
	}

	if person.DiedAt != nil {
		args.DiedAt = sql.NullTime{Time: *person.DiedAt, Valid: true}
	}

	return args, nil
}

//nolint:wsl // cuddle makes code more readable
func FromPeoplePatch(id uuid.UUID, person *model.Person, patch *model.PersonPatch) (*queries.PeopleUpdateParams, error) {
	args := &queries.PeopleUpdateParams{
		ID: id,
	}

	bsn := person.BSN
	if patch.BSN != nil {
		bsn = *patch.BSN
	}
	args.Bsn = sql.NullString{String: bsn, Valid: true}

	name := person.Name
	if patch.Name != nil {
		name = *patch.Name
	}
	args.Name = sql.NullString{String: name, Valid: true}

	sex := person.Sex
	if patch.Sex != nil {
		sex = *patch.Sex
	}
	args.Sex = sql.NullString{String: sex, Valid: true}

	registeredAddressID := person.RegisteredAddressID
	if patch.RegisteredAddressID != nil {
		registeredAddressID = patch.RegisteredAddressID
	}

	if registeredAddressID != nil {
		args.RegisteredAddressID = uuid.NullUUID{UUID: *registeredAddressID, Valid: true}
	}

	bornAt := person.BornAt
	if patch.BornAt != nil {
		bornAt = patch.BornAt
	}

	if bornAt != nil {
		args.BornAt = sql.NullTime{Time: *bornAt, Valid: true}
	}

	diedAt := person.DiedAt
	if patch.DiedAt != nil {
		diedAt = patch.DiedAt
	}

	if diedAt != nil {
		args.DiedAt = sql.NullTime{Time: *diedAt, Valid: true}
	}

	return args, nil
}
