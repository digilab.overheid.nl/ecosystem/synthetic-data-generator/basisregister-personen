package storage

import (
	"context"
	"errors"
	"fmt"
	"net/url"
	"sync"
	"time"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres" // postgres driver
	"github.com/huandu/xstrings"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // postgres driver
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-backend/pkg/storage/migrations"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-backend/pkg/storage/queries/generated"
)

const driverName = "embed"

var registerDriverOnce sync.Once //nolint:gochecknoglobals // We need a singleton pattern

type Database struct {
	DB      *sqlx.DB
	Queries *queries.Queries
}

func New(dsn string) (*Database, error) {
	db, err := NewPostgreSQLConnection(dsn)
	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}

	querier, err := queries.Prepare(context.Background(), db)
	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}

	return &Database{
		DB:      db,
		Queries: querier,
	}, nil
}

func NewPostgreSQLConnection(dsn string) (*sqlx.DB, error) {
	db, err := sqlx.Open("postgres", dsn)
	if err != nil {
		return nil, fmt.Errorf("could not open connection to postgres: %w", err)
	}

	const (
		Five               = 5 * time.Minute
		MaxIdleConnections = 2
	)

	db.SetConnMaxLifetime(Five)
	db.SetMaxIdleConns(MaxIdleConnections)
	db.MapperFunc(xstrings.ToSnakeCase)

	return db, nil
}

func (r *Database) Shutdown() error {
	return r.DB.Close() //nolint:wrapcheck // Not necessary
}

func setupMigrator(dsn, schema string) (*migrate.Migrate, error) {
	registerDriverOnce.Do(func() {
		migrations.RegisterDriver(driverName)
	})

	uri, err := url.Parse(dsn)
	if err != nil {
		return nil, fmt.Errorf("could not parse dsn: %w", err)
	}

	values := uri.Query()
	values.Add("search_path", schema)
	uri.RawQuery = values.Encode()

	migrator, err := migrate.New(fmt.Sprintf("%s://", driverName), uri.String())
	if err != nil {
		return nil, fmt.Errorf("migrate new failed: %w", err)
	}

	return migrator, nil
}

func PostgresPerformMigrations(dsn, schema string) error {
	migrator, err := setupMigrator(dsn, schema)
	if err != nil {
		return err
	}

	if err := migrator.Up(); err != nil {
		if errors.Is(err, migrate.ErrNoChange) {
			fmt.Println("migrations are up-to-date")

			return nil
		}

		return fmt.Errorf("migrator up failed: %w", err)
	}

	return nil
}

func PostgresMigrationStatus(dsn, schema string) (uint, bool, error) {
	migrator, err := setupMigrator(dsn, schema)
	if err != nil {
		return 0, false, err
	}

	version, dirty, err := migrator.Version()
	if err != nil {
		return 0, false, fmt.Errorf("migrator version failed %w", err)
	}

	return version, dirty, nil
}
