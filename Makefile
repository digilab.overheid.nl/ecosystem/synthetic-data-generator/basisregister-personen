dir = $(shell pwd)

default:
	-docker network create digilab-demo-frp

init:
	docker-compose exec fictief-register-personen /api/api migrate init --sdg-postgres-dsn "postgresql://postgres:postgres@postgres:5432/frp?sslmode=disable"

migrate-up:
	docker-compose exec fictief-register-personen /api/api migrate up --sdg-postgres-dsn "postgresql://postgres:postgres@postgres:5432/frp?sslmode=disable"

build:
	docker-compose build

up: default sqlc
	docker-compose up -d --remove-orphans

logs:
	docker-compose logs -f

down:
	docker-compose down --remove-orphans

sqlc:
	docker run --rm -v $(dir)/pkg/storage:/src -w /src/queries kjconroy/sqlc:1.20.0 generate

psql:
	docker-compose exec postgres psql -U postgres
